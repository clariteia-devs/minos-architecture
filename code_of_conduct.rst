Code Of Conduct
===============

aqui se describiran las reglas generales de desarollo que todos los desarolladores tendran que cumplimentar

Semantic Versioning
-------------------

el Framework como cada Package tendran que tener una semantic_version_.

- Una version normal de software tendra siempre el siguiente formato X.Y.Z. Donde X es una version Major, Y es una version Minor y z es una PATCH
- La Major version 0.Y.Z es la version de desarollo
- la Version 1.0.0 es la version publica
- la version patch x.y.Z con x > 0 podran ser incrementadas solo si hay bug fixes compatibles con la version precedente
- la Minor version x.Y.z con x > o podran ser incrementadas solo si hay funcionalidades compatibles con las versiones precedentes
- la Major version X.y.z con x > 0 podran ser incrementadas solo se introducen cambios **incompatibles** con las versiones precendetes
- Una prerelease se identificará como 1.0.0-alpha o 1.0.0-beta


para mayores informaciones leer el documento en este link semantic_version_

Comments and Docstrings
-----------------------

Modules
^^^^^^^

Cada fichero tiene que empezar con el siguiente codigo de licencia

.. code-block:: python

    # Copyright (C) 2020 Clariteia SL
    #
    # This file is part of minos framework.
    #
    # Minos framework can not be copied and/or distributed without the express
    # permission of Clariteia SL.

cada fichero, despues de la licencia tiene que empezar con una descripcion del contenido y su uso

.. code-block:: python

    """A one line summary of the module or program, terminated by a period.

    Leave one blank line.  The rest of this docstring should contain an
    overall description of the module or program.  Optionally, it may also
    contain a brief description of exported classes and functions and/or usage
    examples.

      Typical usage example:

      foo = ClassFoo()
      bar = foo.FunctionBar()
    """


Funciones y Metodos
^^^^^^^^^^^^^^^^^^^

Una funcion o un metodo no necesitan un comentario si:

- no son visibles externamente
- muy cortas
- obvias

la documentacion de una funcion tiene que dar informacion necesaria para poder lanzar la funcion o llamar el
metodo sin necesidad de leer el codigo

un metodo sobrescrito no necesita una descripcion

.. code-block:: python

    def fetch_smalltable_rows(table_handle: smalltable.Table,
                          keys: Sequence[Union[bytes, str]],
                          require_all_keys: bool = False,
    ) -> Mapping[bytes, Tuple[str]]:
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            table_handle: An open smalltable.Table instance.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: Optional; If require_all_keys is True only
              rows with values set for all keys will be returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """

hay siempre que describir:

- Args
- Returns
- Raises: esto si el metodo devuelve un error en algun momento

Clases
^^^^^^
La clase tendria que tener documentacion por debajo de la definicio de clase

.. code-block:: python

    class SampleClass:
    """Summary of class here.

    Longer class information....
    Longer class information....

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """

    def __init__(self, likes_spam=False):
        """Inits SampleClass with blah."""
        self.likes_spam = likes_spam
        self.eggs = 0

    def public_method(self):
        """Performs operation blah."""

Commentarios
^^^^^^^^^^^^

Estan admitidos comentarios al codigo, sobretodo para explicar procesos complejos, intentar no comentar con obviedades

.. code-block:: python

    # We use a weighted dictionary search to find out where i is in
    # the array.  We extrapolate position based on the largest num
    # in the array and the array size and then do binary search to
    # get the exact number.

    if i & (i-1) == 0:  # True if i is 0 or a power of 2.

TODO Comments
^^^^^^^^^^^^^
Esta aceptado añadir comentarios de tipo TODO, pero:

- Es un comentario temporal
- No es excesivamente descriptivo
- Es aconsejable siempre poner un TODO con una referencia a un desarollador o un responsable

.. code-block:: python

    # TODO(Zeke) Change this to use relations.



Typing
^^^^^^

Python ya acepta el tipado, es obligatorio, en toda la medida delo posible usar tipado



.. _semantic_version: https://semver.org/
