Message
=======

El mensaje es una parte simple pero muy importante a la hora de mover eventos y ejecutar comandos remotos

Se utiliza un patron especifico message_pattern_ ( en este documento hay una explicacion muy groso modo ).

En Minos hay tres tipos de mensajes

- Document: mensaje generico que contiene solo datos. El recevier decide el uso que hacer con este mensaje. Un ejemplo es la respuesta de un comando
- Command: mensaje que coresponde a un comando RPC. especifica la operacion a ejecutar
- Event: mensaje que informa que algo importante ha ocurrido. En *Minos* el event es principalmente un *Domain Event*, que representa el cambio de estado de un *Domain Object*

Message Structure
^^^^^^^^^^^^^^^^^

Un Mensaje esta compuesto principalmente por:

- Headers: Key/Value Store.
    - MessageId: id Unico
    - CorrelationId: contiene MessageId si el mensaje es una respuesta
    - ReturnAddress: (Optional) Nombre del Channel de Respustesta, este campo aparece solo en las Requests
- Body: contenido del mensaje









.. _message_pattern: https://www.enterpriseintegrationpatterns.com/patterns/messaging/Message.html