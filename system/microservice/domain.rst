Domain
======

En el Domain Design Driven, lo primero en que te tienes que focalizar es el Business del cliente.
Tienes que ver, segun el proyecto que tienes cual es el Negocio que tienes que desarollar con tu
producto.

El Dominio es lo que la empresa hace y lo que el mundo hace con el.

El Domain de una empresa, obivamente esta compuesto por diferentes componentes estos componentes son
los SubDomain

Por ejemplo una empresa minorista que vende productos en línea.

Los productos que vende pueden ser casi cualquier cosa, por lo que no pensaremos demasiado en ellos.

Para hacer negocios en este Dominio, la empresa debe presentar un catálogo de productos a los compradores,
debe permitir que se realicen pedidos, debe cobrar el pago por los productos vendidos y
debe enviar los productos a los compradores. El dominio de este minorista en línea parece estar compuesto
por estos cuatro subdominios principales:

- **catálogo de productos**
- **pedidos**
- **facturación**
- **envío**.

.. image:: /img/domain.png

en el diagrama de arriba vemos que el Domain Business contiene una serie de SubDomains. Tambien se puede apreciar que
dentro del Domain hay otras *Section* de linea gruesa y continua que crean un espacio con una serie de
SubDomains. Este contenedor de SubDomains se llama **Bounded Contexts**, por motivo de tiempo, no vamos
a detallar para que sirve un Bounded Context, pero como resumen, podemos decir que un Domain tiene que
dividir sus SubDomains, internamente, y esto aumenta la complejidad del Producto.
Para lidiar con esta complejidad es necesario subdividir el dominio en "Zonas" que tengan dentro uno o mas Subdominios
y que tengan reglas tan diferentes entre ellas que se tienen que tratar en manera diferente, aunque pertenezcan
al mismo Domain. El caso es lo del Inventory, que es un producto dentro del producto, con una complejidad, por
asi decir, completamente diferente de la que tiene el Bounded Context del E-Commerce.

Las conexiones entre los **Bounded Context** se llaman **Context Map**.

Estos ultimo son utile por un motivo principal:

- Cuando hay que conectar, por ejemplo, un sistema de microservicios con un sistema Monolito ya presente en la red del cliente ( llamado **Big Ball of Mud** ), visualizar graficamente quien se conecta al Monolito y porque

Estas visiones a alto nivel, nos ayudan, a diseñar el producto respectando las reglas de Negocio impuestas por
el cliente.
Ademas, en la fase de diseño del sistema de microservicios ayuda a identificar mejor las
:doc:`Entities </system/microservice/entities>` y los :doc:`Aggragates </system/microservice/aggregate>`