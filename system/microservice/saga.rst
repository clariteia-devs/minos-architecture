Saga
====

Las SAGAS son necesarias, en una arquitectura de microservicios para mantener el *data consistency* sobretodo
cuando se estan utilizando transaciones entre servicios.
En particular las SAGAS se pueden considerar ACD y no ACID, osea que vendria a faltar la parte de *Isolation*.
La falta de *Isolation* puede causar algunas problematicas:

- *Lost Updated*: cuando la SAGA actualiza un dato que mientras tanto ha sido actualizado por otro proceso
- *Dirty Reads*: una transacion lee un dato que ha sido actualizado por una SAGA que aun no ha terminado
- *Fuzzy/nonrepeatable reads*: Dos pasos de la misma SAGA leen el mismo dato pero con dos valores diferentes porque otra SAGA, ha actualizado el dato mientras tanto

existen varias soluciones para lidiar cone ste problema pero actualmente no se va a aplicar en la version 1.0.0 de minos


representamos graficamente como funciona un proceso de SAGA

.. uml::

    participant "Saga" as saga
    participant "Order Service" as order
    participant "Consumer Service" as consumer
    participant "Kitchen Service" as kitchen
    participant "Accounting Service" as accounting

    saga -> saga : Start Process
    saga -> order: Txn 1 <<Create Order>>
    saga -> consumer: Txn 2 <<Verify Consumer>>
    saga -> kitchen: Txn 3 <<Create Ticket>>
    saga -> accounting: Txn 4 <<Authorize Card>>
    saga -> kitchen: Txn 5 <<Approve Ticket>>
    saga -> order: Txn 6 <<Approve Order>>

La SAGA es una secuencia de comandos internos o externos al servicio para completar un proceso de business logic.
Las SAGA no son ACID o por lo menos no cumplimentan todos los aspectos de ACID.
Una base de datos, aunque envia una serie de transaciones a sus tablas, en caso de problemas llamará el comando
*ROLLBACK* y todo volverá al estado inicial, esto no pasa en las SAGA, por ese motivo es necesario introducir un proceso
de Compensacion

Saga Pseucode
-------------

.. code-block:: console

   this.sagaDefinition=
        step()
        .withCompensation(...)
        .step()
        .invokeParticipant(...)
        .step()
        .invokeParticipant(...)
        .onReply(...)
        .withCompensation(...)

Compensating Transactions
-------------------------

El principio es relativamente simple

si se han enviado (n) transaciones y (n+1) falla, los efectos de las n transaciones enviadas tiene que ser deshechos..
La SAGA en caso de fallo, tiene que tener conocimiento de los procesos de Compensacion de las (n) transaciones y
volver a ejecutar a la inversa.
No todos los pasos de una SAGA necesitas una compensacion.

=====  =================  ================= ========================
Step   Service            Transaction       Compensating Transaction
=====  =================  ================= ========================
1      Order Service      createOrder()     rejectOrder()
2      Consumer Serv.     verifyConDetail() -----
3      Kitchen Serv.      createTicket()    rejectTicket()
4      Accounting Serv.   authCreditCard()  -----
5      Kitchen Serv.      approveTicket()   -----
6      Order Service      approveOrder()    -----
=====  =================  ================= ========================

Saga Orchestrator
-----------------

La organizacion de los procesos de la SAGA occurre a traves de un patron llamado **SAGA Orchestrator**

El proceso consiste en que el Servicio que crea la SAGA será el orquestrador de todos los eventos generados

.. uml::

    top to bottom direction

    artifact "Service" {
        component "Create Order SAGA Orchestrator" as create
    }
    artifact "Message Broker" {
        queue "Consumer Request Channel" as consumer_request
        queue "Create Order Reply Channel" as create_order_reply
        queue "Kitchen Request Channel" as kitchen_request
        queue "Accounting Request Channel" as accounting_request
        queue "Order Request Channel" as order_request

        consumer_request -[hidden]down-> create_order_reply
        create_order_reply -[hidden]down-> kitchen_request
        kitchen_request -[hidden]down-> accounting_request
        accounting_request -[hidden]down-> order_request
    }

    artifact "Microservices" {
        component "Consumer Service" as consumer
        component "Kitchen Service" as kitchen
        component "Accounting Service" as accounting
        consumer -[hidden]down-> kitchen
        kitchen -[hidden]down-> accounting
    }

    create .> consumer_request : 1 Verify Consumer
    consumer_request .> consumer
    consumer -> create_order_reply : 2 <<Consumer Verified>>
    create_order_reply -> create

    create .> kitchen_request : 3, 7 Create Ticket/Approve Rest Order
    kitchen_request .> kitchen
    kitchen -> create_order_reply: 4 <<Ticket Created>>

    create .> accounting_request : 5 Authorize Card
    accounting_request .> accounting
    accounting -> create_order_reply : 6 <<Card Authorized>>

    create .> order_request : 8 Approve Order
    order_request .> create

Saga Manager
------------

Siendo la SAGA un proceso asincrono que, ademas, involucra servicios externos es necesario tener una sesion
de las SAGAs que se estan ejecutando, para esto entra en juego el SAGA Manager


.. uml::

    top to bottom direction

    artifact "Microservice" {
        component "Service" as service
        component "Saga Manager" as saga_manager
        component "Aggregate" as aggregate
        component "Repository" as repository
        component "Proxy Microservice 1" as proxy_one
        component "Proxy Microservice 2" as proxy_two
        component "Command Manager" as command_manager

        service -down-> saga_manager
        service -down-> aggregate
        aggregate -down-> repository

        saga_manager -right-> proxy_one
        saga_manager -down-> proxy_two

        proxy_one -down-> command_manager
        proxy_two -left-> command_manager

        command_manager -down-> service
        command_manager -down-> saga_manager
    }

    queue "Service Requests" as queue_service
    queue "Microservice 1 Requests" as queue_micro_one
    queue "Microservice 2 Requests" as queue_micro_two
    queue "Service Saga Replies" as queue_service_reply

    queue_service -[hidden]down-> queue_micro_one
    queue_micro_one -[hidden]down-> queue_micro_two
    queue_micro_two -[hidden]down-> queue_service_reply

    queue_service -left-> command_manager
    command_manager -right-> queue_micro_one
    command_manager -right-> queue_micro_two
    queue_service_reply -left-> command_manager


el servicio recibe un comando desde el controller y lanza una nueva instancia de SAGA

.. uml::

    participant "Controller" as controller
    participant "Service" as service
    participant "Saga" as saga
    database "Saga Table" as table_saga

    controller -> service: receive a command
    service -> saga: start a SAGA process
    saga -> saga: generate Unique ID
    saga -> table_saga: add the SAGA instance


El SAGA Manager se encarga ademas de arrancar el proceso SAGA specifico.

.. uml::

    participant "Controller" as controller
    participant "Service" as service
    participant "Saga Manager" as manager
    participant "Saga Definition" as definition
    database "Saga Instance" as table_saga
    database "Saga State" as state

    controller -> service: receive a command
    service -> manager: get the command
    manager -> manager: generate unique ID
    manager -> table_saga: create instance
    manager -> definition: get the instance of the SAGA
    definition -> manager: instance
    manager -> manager: start the execution
    manager -> definition: execute steps
    definition -> definition: next()
    definition -> state: update the SAGA state


el SAGA Manager, segun el tipo de comando recibido, tiene que tener conocimiento de cual es la SAGA Definition, osea
cuales son los pasos a seguir. Este proceso esta a cargo del Saga Definition

Una ejecucion de una SAGA implica comunicar con Microservicios externos pero tambien tiene que recibir respuestas
de los step que se estan ejecutando.

.. uml::

    queue "Service SAGA Response" as queue_response
    participant "Command Manager" as command
    participant "Saga Manager" as manager
    participant "Saga Definition" as definition
    database "Saga Instance" as table_saga
    database "Saga State" as state

    queue_response -> command: receive the response
    command -> manager
    manager -> table_saga: check the saga id
    manager -> definition: resume the SAGA Process
    definition -> state: get the latest state
    definition -> definition: resume
    definition -> state: update the state

SAGA Definition
^^^^^^^^^^^^^^^

La SAGA Definition es una clase, que normalmente tiene el nombre del servicio, con dentro todas las SAGAs que
tendran que ser ejecutadas

.. code-block:: console

    class OrderSagaDefinition

        method createOrder()
            this.sagaDefinition.
                step()
                .withCompensation(...)
                .step()
                .invokeParticipant(...)
                .step()
                .invokeParticipant(...)
                .onReply(...)
                .withCompensation(...)

        method start()
            this.sagaDefinition.start

el codigo de la SAGA definition se autogenera durante la fase de desarollo, el :doc:`Cli </system/cli>` ofrece una
serie de parametros y flags para autogenerar el codigo de SAGA Definition

el desarollador tendra que crear una estructura xml para la creacion de la saga, por el momento reportamos informacion
parcial de los parametros del XML, mas adelante crearemos un apartado especifico de especificacion.

.. code-block:: xml

    <saga>
        <name>Order</name>
        <step>
            <proxy>Kitchen</proxy>
            <command>addTicket</proxy>
            <params>
                <param name="order_id" value="int">
            </params>
            <compensation>
                <command>removeTicket</proxy>
                <params>
                    <param name="ticket_id" value="int">
                    <param name="order_id" value="int">
                </params>
            </compensation>
        </step>
    </saga>

Microservices Proxy
^^^^^^^^^^^^^^^^^^^

En el SAGA definition una parte importante lo tienen los Proxy con los Microservicios externos.
los Proxy comunican a traves del :doc:`Command Manager </system/microservice/command>` y son
necesarios a crear los comandos que seran enviado al broker para obtener informacion de los microservicios externos

.. uml::

    participant "Saga Definition" as definition
    participant "Proxy" as proxy
    participant "Command Manager" as command
    queue "Microservice Request Queue" as queue

    definition -> proxy: step/compensation action
    proxy -> command: command message
    command -> queue


