Command Manager
===============

El command manager se encarga de las comunicaciones con el Broker para gestionar las Requests/Responses

Es principalmente usados por las :doc:`Sagas </system/microservice/saga>`


.. uml::

    top to bottom direction

    artifact "Microservice" {
        component "Service" as service
        component "Command Handler" as handler
        component "Command Publisher" as publisher
        component "Command Replies" as replies
        component "Saga Manager" as manager
        component "Service Proxy" as proxy

        service -down-> manager
        manager -down-> proxy
        proxy -down-> publisher
    }

    queue "Service Request" as service_request
    queue "SAGA Responses" as saga_responses
    queue "Others Services Requests" as other_services_request

    service_request -> handler
    handler -down-> service
    saga_responses -> replies
    replies -left-> manager
    publisher -> other_services_request

En realidad el command manager es un conjunto de componentes, que tienen que gestionar las Requests/Responses.

Command Handler
---------------

El command handler gestiona los comandos recibidos por la queue de request

la queue de las request es un topic con un nombre compuesto por el nombre del servicio mas la palabra *command_requests*

ej.

si tenemos un servicio llamado Order, el nombre de la queue de las requests sera *order_command_requests*


cada microservicio tendra una queue de gestion de las command requests

El Command Handler redirecionará solo al Service Component, que es donde reside la business logic


Saga Replies
------------

Este componente se encarga de recibir las respuesta y de revertirlas hacia el SAGA Manager. despues se ejecutará
el resume de la SAGA


Command Publisher
-----------------

Este componente es la conexion hacia las cola de las *Requests* de los otros microservicios.

El Command Publisher sabe a que queue tiene que enviar el comando desde el Proxy que le esta llamando
En linea general el nombre del proxy mas *command_requests* indica la queue

