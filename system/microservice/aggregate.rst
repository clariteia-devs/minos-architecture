Aggregate
=========

Para entender que es un Aggregate y como se puede crear uno es necesario tener claro que son

- :doc:`Entity </system/microservice/entities>`
- :doc:`Object Value </system/microservice/object_value>`
