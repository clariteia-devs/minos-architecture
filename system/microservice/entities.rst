Entities
========

La Entity es un objecto que puede cambiarse a lo largo del tiempo muchas veces, hasta llegar a ser un objeto completa
mente diferente de lo inicial, pero tendrá siempre una identidad fija y inmutable

en un diseño de base datos una Entity es un objecto que se reconoce por su ID, el resto de los atributos pueden
modificar su valor pero el ID siempre es lo mismo y siempre se identificará esa Entity con ese ID.

La generacion del ID para la entidad puede ser de difertentes maneras:

- Desde el usuario, por ejemplo con la crecion de una llave o un nombre unico
- Desde la aplicacion, por ejemplo con algoritmos de criptacion
- Desde la base de datos, generacion del ID automatica


Es importante tener presente, durante el diseño de las Entities que en un Domain existe siempre

- Una Entity Root
- (Opcional) una serie de :doc:`Object Values </system/microservice/object_value>`

Una Entity Root es la Entity que gestiona multiples Object Values y que tiene referencias directas con otras
Entities Root, en Minos la Entity Root es la base del :doc:`Object Values </system/microservice/aggregate>`.


A nivel de codigo una Entity tiene atributos con sus Getter y Setter, pero el ID seria buena idea desarollarlo solo con
un Getter, en esta manera se reduce el riesgo de modificacion.

.. uml::

    class "User <<entity>>" as user {
        username: String
        password: String
        changePassword()
        changePersonalName()

    }

    class "Tenant <<object value>>" as tenant {
    }

    class "Person <<entity>>" as person {
    }

    class "Contact Info <<object value>>" as contact {
    }

    class "Name <<object value>>" as name {
    }

    user -up-> tenant
    user -> person
    person -down-> contact
    person -down-> name

**La Entity tiene tambien la responsabilidad de la validacion de los datos.**

