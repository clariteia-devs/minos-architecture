Object Value
============


Para identificar si un objecto es un Object Value hay que tener presente las sigueintes reglas:

- Mide o cuantifica o describe algo en un dominio

- Se puede mantener inmutable

- Modela un concepto al componer atributos relacionados con una unidad ( Entity )

- Es posible remplazarlo cuando una medida o una descripcion cambia

- es posible compararlo con otros utilizando un Valor

En :doc:`Entity </system/microservice/entities>` hay una descripcion de como se pueden crear unos Object Values y
conectarlos a una Entity

