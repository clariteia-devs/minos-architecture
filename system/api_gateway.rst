Api Gateway
===========

El API Gateway tiene dos vertiente importantes:

- interfaz para las aplicaciones
- enrutador de las llamadas hacia los microservicios

El Api Gateway ademas contiene una serie de servicio añadidos:

- Discovery Service
- Identity Service

El api Gateway esta compuesto por diferentes servicios:

- Mobile API
- Browser API
- Public API

.. uml::

   top to bottom direction

   package "API gateway"{
        component "Identity Provider" as identity
        component "Service Discovery" as discovery

       () "HTTP" as http_discovery
       queue "Queue" as queue
       discovery -left- http_discovery
       discovery - queue

       component "Mobile API" as mobile{
            () "HTTP" as mhttp
            component "REST" as mrest
            component "Common Library" as mcommon
            mhttp - mrest : use
            mrest <-down- mcommon
            mcommon <..> queue
       }

       component "Browser API" as browser{
            () "HTTP" as bhttp
            component "REST" as brest
            component "Common Library" as bcommon
            bhttp - brest : use
            brest <-down- bcommon
            bcommon <..> queue
       }

       component "Public API" as public{
            () "HTTP" as thttp
            component "REST" as trest
            component "Common Library" as pcommon
            thttp - trest : use
            trest <-down- pcommon
            pcommon <..> queue
       }

       mcommon -[hidden]down- discovery
       bcommon -[hidden]down- discovery
       pcommon -[hidden]down- discovery

       mcommon -[hidden]down- identity
       bcommon -[hidden]down- identity
       pcommon -[hidden]down- identity

   }


cada servicio internamente tendrá:

- REST Interface
- Common Library

Discovery Service
-----------------

El Discovery Service actua como si fuera un DNS.

Cada vez que se despliega un microservicio es posible que sus interfaces cambien ( IP / Domain ), para eso es
necesario tener un servicio que monitoriza esos cambios

usamos el client_side_pattern_

Subscription
^^^^^^^^^^^^

.. uml::


   == Service Startup ==

   Microservice -> "Discovery Service" : /subscribe POST
   database "Redis" as redis
   queue "Kafka" as queue
   "Discovery Service" -> redis : update IP/Name/Protocol/Route status: true

   "Discovery Service" -> queue : event <<service_sub>> IP/Name/Protocol/Route/Status

   "Discovery Service" --> Microservice : 200 OK


Los API Gateway reciben el evento y actualizan su lista de servicios

.. uml::

   queue "Kafka" as queue

   queue -> "API Gateway" : event <<service_sub>>
   database "Redis" as redis
   "API Gateway" -> redis : update table of services

Removal
^^^^^^^

.. uml::


   == Service Shutdown ==

   Microservice -> "Discovery Service" : /unsubscribe POST
   database "Redis" as redis
   queue "Kafka" as queue
   "Discovery Service" -> redis : update IP/Name/Protocol/Route status: false

   "Discovery Service" -> queue : event <<service_unsub>> IP/Name/Protocol/Route/Status

   "Discovery Service" --> Microservice : 200 OK

.. uml::

   queue "Kafka" as queue

   queue -> "API Gateway" : event <<service_unsub>>
   database "Redis" as redis
   "API Gateway" -> redis : update table of services


Rounting
--------

El proceso de enrutamiento sigue las lineas de api_gateway_

el enrutamiento ocurre por *path* de la url

ej.

http://www.testdomain.com/orders

El API gateway podra redirecionar la llamada al proxy interno que identificará el *path* orders y
tendrá ya el cliente preparado para llamar el microservicio correspondiente.
El proceso de creacion de los comandos HTTP será automatizado con el :doc:`Cli </system/cli>`

.. uml::

    actor "User" as user
    participant "Application" as app
    participant "Api Gateway" as api
    participant "Proxy" as proxy
    participant "Router" as router
    participant "Microservice" as micro
    database "Redis" as redis

    user -> app : POST /order
    app -> api : POST /order
    api -> proxy: POST /order
    proxy -> router: order route POST
    router -> redis: from "order" get ip/port
    router -> micro: redirect POST /order

para un GET el proceso seria


.. uml::

    actor "User" as user
    participant "Application" as app
    participant "Api Gateway" as api
    participant "Proxy" as proxy
    participant "Router" as router
    participant "Microservice" as micro
    database "Redis" as redis

    user -> app : GET /orders
    app -> api : GET /orders
    api -> proxy: GET /orders
    proxy -> router: order route GET
    router -> redis:  from "orders" get ip/port
    router -> micro: redirect GET /orders
    micro --> router
    router --> proxy
    proxy --> api
    api --> app

Gestion de Errores
^^^^^^^^^^^^^^^^^^

Si durante el enrutamiento el microservicio no contesta el proxy del API Gateway activa un circuit_breaker_.

.. uml::

    actor "User" as user
    participant "Application" as app
    participant "Api Gateway" as api
    participant "Proxy" as proxy
    participant "Router" as router
    participant "Microservice" as micro
    database "Redis" as redis

    user -> app : GET /orders
    app -> api : GET /orders
    api -> proxy: GET /orders
    proxy -> router: GET /orders
    router -> redis: from "orders" get ip/port
    router -> micro !! : redirect GET /orders
    router --> proxy : 500 fail
    proxy --> proxy : cache the call
    proxy -> proxy : timeout Ej, 1 sec
    proxy -> router : GET /status
    router -> micro
    micro --> router: 200 Ok
    router --> proxy: 200 Ok
    proxy --> proxy: reset timeout
    proxy -> proxy: read cache
    proxy -> router: redirect GET /orders
    router -> micro: GET /orders
    micro --> router
    router --> proxy
    proxy --> api
    api --> app


Router Pseudo Code
------------------


.. code-block:: console

    #order file
    function on_get(params)
        pass the parameter path to a http client.

        return the results

    function on_post(params)
        pass the parameter path to the http client

        return 201 OK or Fail



.. _client_side_pattern: https://microservices.io/patterns/apigateway.html
.. _api_gateway: https://microservices.io/patterns/apigateway.html
.. _circuit_breaker: https://microservices.io/patterns/reliability/circuit-breaker.html