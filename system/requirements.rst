Requisitos
==========

Requisitos Ordenador
--------------------

En primer lugar actualizar el xcode

.. code-block:: console

   $ xcode-select --install

Amazon AWS CLI V2
-----------------

seguir las instruciones en este link awscli_

consola Amazon AWS de clariteia : consoleAWS_

despues de instalar la consola es necesario configurar aws cli:

.. code-block:: console

   $ aws configure

se os pedirá la AccessKey y la AccessSecret. Estas dos informaciones se pueden sacar desde la Console de Amazon.

.. note::

    se os proveerá de una AccessKey y de una AccessSecret

Amazon Kubernetes configuration
-------------------------------

para poder configurar el *kubectl* es necesario ejecutar el siguiente comando

.. code-block:: console

    aws eks --region eu-region-1 update-kubeconfig --name clariteia

despues de esto ya es posible ejecutar los comandos de Kubectl


Eksctl configuration
--------------------

Instalacion del comando eksctl

** es necesario instalar antes Homebrew **

.. code-block:: console

   $ brew tap weaveworks/tap

.. code-block:: console

   $ brew install weaveworks/tap/eksctl

para verificar que todo se ha cumplido correctamente:

.. code-block:: console

   $ eksctl version


Oh-My-ZSH
---------

puedes encontrar todos los plugin en este link ohmyzsh_.

.. code-block:: console

   $ sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"


si quieres instalar un plugin tienes que abrir ${HOME}/.zshrc i buscar *plugins*.

**Antes**

.. code-block:: console

   $ plugins=(git)

**Despues**

.. code-block:: console

   $ plugins=(git otherplugin)


Homebrew
---------

.. code-block:: console

   $ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

PyEnv
-----

.. code-block:: console

   $ brew update
   $ brew install pyenv

despues de la instalacion es necesario añadir el plugin a oh-my-zsh

.. code-block:: console

   $ plugins=(git pyenv)

Python 3.9
----------

despues de instalar Pyenv podemos instalar Python 3.9

para la version de OSX Mojave es necesario añadir algunos flags antes de instalar Python

.. code-block:: console

   $ CONFIGURE_OPTS="--with-openssl=$(brew --prefix openssl)" CFLAGS="-I$(brew --prefix zlib)/include -I$(brew --prefix sqlite)/include" LDFLAGS="-L$(brew --prefix zlib)/lib -L$(brew --prefix sqlite)/lib" pyenv install 3.9.0

activamos a nivel global python 3.9

.. code-block:: console

   $ pyenv global 3.9.0


Amazon CodeArtifact ( PIP Privado )
-----------------------------------

en clariteia tenemos un PIP privado donde se suben los packages que estan relacionados con Minos.

Para poder usar estos packages es necesario configurar el pip con la nueva url

.. code-block:: console

    aws codeartifact login --tool pip --repository minos --domain pip-clariteia --domain-owner 785264909821

este comando configura pip para 12 horas, cada 12 horas es necesario reiniciarlo para poder obtener un token nuevo.

despues de esto es posible comprobar si la configuracion es correcta ejecutanto

.. code-block:: console

    pip install minos-poetry-dependencies-test

si se descarga el package todo funciona correctamente, despues se puede borrar el package.

Amazon CodeArtifact ( Subir Package )
-------------------------------------

Para subir un package al repositorio privado es necesario ejecutar el Makefile que todos los packages de Minos tienen
que tener obligatoriamente.

el comando es:

.. code-block:: console

    make release

si el aws esta configurado correctamente este comando subirá directamente el package en el repositorio privado


Cookiecutter
------------

para el desarollo es necesario instalar algunas dependencias de python

.. code-block:: console

   $ pip install --user cookiecutter

Graphviz
--------

para visualizar correctamente la documentacion en Spinx del proyecto es necesario instalar a nivel
global algunas dependencias, entre ellas Graphviz

.. code-block:: console

   $ brew install libtool
   $ brew link libtool
   $ brew install graphviz
   $ brew link --overwrite graphviz





.. _awscli: https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html#cliv2-mac-prereq
.. _ohmyzsh: https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins
.. _consoleAWS: https://785264909821.signin.aws.amazon.com/console