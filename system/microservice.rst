Microservice
============

El microservicio es un conjunto, bastante articulado de diferentes patrones

.. image:: /img/micro_patterns.png

y como en la arquitectura de sistema, tambien el microservicio utiliza una arquitectura exagonal

.. image:: /img/microservice_exagonal.png

En mas detalle, un microservicio de Minos contiene una serie de componentes y interfaces ( Outbound/Inbound), que
se pueden representar en esta manera:

.. uml::

   top to bottom direction

   component Microservice {
        component "REST API" as rest
        component "Event Manager" as event_manager
        component "Service" as service
        component "Saga Manager" as saga_manager
        component "Command Manager" as command_manager
        component "Aggregate" as aggregate
        component "Repository" as repository
        component "Database Adapter" as dbadapter

        rest -down-> service
        event_manager -left-> service
        command_manager -down-> saga_manager
        service -down-> aggregate
        service -down-> repository
        repository -down-> dbadapter
        saga_manager -> service
   }

En un microservicio hay las siguientes Interfaces

- :doc:`REST </system/microservice/rest>`
- Event
- Command

El REST tiene representa el Subdomain del microservicio. tiene que suportar los comandos HTTP. EL REST

La arquitectura de Microservicios de Minos es *Event Driven*, para esto la primera cosa a tener en cuenta es
el formato del :doc:`Mensaje </system/microservice/message>` que se tiene que enviar.


Sabiendo el formato de los :doc:`Mensajes </system/microservice/message>` ahora es necesario ver en que manera
es posible transmitirlos y recibirlos.
Para esto usamos un metodo Asincrono de Request/Response


.. uml::

    top to bottom direction

    queue "Request Channel" as request_channel
    component "Service 1" as service_one
    component "Service 2" as service_two
    queue "Response Channel" as response_channel

    service_one -up-> request_channel : 1 Sends
    request_channel -down-> service_two : Reads
    service_two -down-> response_channel : 2 Sends
    response_channel -up-> service_one : Reads

El microservicio envirá un mensaje (1) con la siguiente estructura

.. uml::

    object message {
        message_id = 123456
        response_address = "service_response_channel"
        body = "some message"
    }

la respuesta (2) será

.. uml::

    object message {
        correlation_id = 123456
        body = "other info or null"
    }

Ahora tenemos claro en que manera hay que enviar los mensajes y en que formato.

Queda aclarar en que manera el microservicio maneja los diferentes formatos de mensajes

- :doc:`Command Manager </system/microservice/command>`
- :doc:`Event Manager </system/microservice/event>`


.. toctree::
    :hidden:

    microservice/domain
    microservice/rest
    microservice/event
    microservice/message
    microservice/saga
    microservice/command
    microservice/aggregate
