Docker
======

Para docker usamos el Amazon Elastic Container Registry.

para poderlo usar tenemos que autentificarnos antes
es necesario tener instalado el aws cli


.. code-block:: console

    aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 785264909821.dkr.ecr.eu-central-1.amazonaws.com


este es el ejemplo de login del registry de clariteia que es el default registry, en el caso este la necesidad de configurar
otro, cambiar la direcion al final del comando

Crear un Registry
-----------------

Antes de hacer un push es necesario confirmar si existe el registry de la imagen que se quiere enviar.

Si no existiera es necesario crearla con

.. code-block:: console

    aws ecr create-repository --repository-name mytest

imaginamonos de tener una imagen con el nombre mytest, este es el comando para crearla


Docker Build
------------

despues de tener el registry creado podemos hacer build de la imagen

.. note::

    es importante recordar de lanzar los comandos dentro de la carpeta que contiene el Dockerfile


.. code-block:: console

    docker build -t 785264909821.dkr.ecr.eu-central-1.amazonaws.com/mytest:0.0.1 .

esto crea una imagen con el tag que coresponde a la direcion del repositorio de default de clariteia con el nombre y la
version de la imagen

Docker Push
-----------

Despues de hacer el build de la imagen es necesario subir la imagen en el Container Registry

.. code-block:: console

    docker push 785264909821.dkr.ecr.eu-central-1.amazonaws.com/mytest:0.0.1

Docker Pull
-----------

despues de subir la imagen podemos usar el docker desde el tag de la imagen

.. code-block:: console

    docker pull 785264909821.dkr.ecr.eu-central-1.amazonaws.com/mytest:0.0.1

Es posible, tambien llamar la imagen desde kubernetes tambien