Architectura de Sistema
=======================

Minos es una red de microservicios.

Una arquitectura de microservicios utiliza multiples patrones para los diferentes layers del sistema.

.. image:: /img/micro_patterns.png
   :align: center

es importante tener en cuenta que una arquitectura de microservicios nos ofrece:

- Scalability
- Availability
- Resiliency
- Flexibility
- Independent, autonomous
- Decentralized governance
- Failure isolation
- Auto-Provisioning
- Continuous delivery through DevOps

Los microservicios tienen que ver con hacer que los servicios estén *loosely coupled*,
aplicando el principio de responsabilidad única. Se descompone por capacidad empresarial.

Para entender como desarollar una solucion de microservisios hay que empezar con entender que es un
:doc:`Dominio </system/microservice/domain>`.

a alto nivel la arquitectura esta compuesta por diferentes *Layers*:

- User Interface Layer
- Application Layer
- Domain Layer
- Infrastructure Layer

pero para poder entender mejor como funciona una solucion de microservicios, resulta mas simple volver
a diseñar la red en manera exagonal o una arquitectura exagonal:

.. image:: /img/exagonal.png

Este tipo de arquitectura exagonal es aplicable al diseño de la solucion en su totalidad como
al diseño de la arquitectura del :doc:`Microservicio </system/microservice>` en si.

En la Arquitectura de sistema cabe evidenciar el rol que tienen los Adapters.
Estos son interfaces hacia el exterior que permiten abrir la comunicacion entre las User Interfaces y los
microservicios, estos Adapter son las :doc:`API Gateway </system/api_gateway>`.

En el grafico podemos ver como estas interfaces de comunicacion conectan la UI con los microservicios.

.. uml::

   top to bottom direction

   package Application {
       [Mobile Application]
       [Browser Application]
       [Public Application]
   }

   package "API gateway" as pgateway{
       component "Service Discovery" as discovery
       component "Identity Provider" as identity

       component "Mobile API" as mobile{
            () "HTTP" as mhttp
            component "REST" as mrest
            component "Common Library" as mcommon
            mhttp - mrest : use
            mrest <-down- mcommon
            [Mobile Application] --> mhttp
       }

       component "Browser API" as browser{
            () "HTTP" as bhttp
            component "REST" as brest
            component "Common Library" as bcommon
            bhttp - brest : use
            brest <-down- bcommon
            [Browser Application] --> bhttp
       }

       component "Public API" as public{
            () "HTTP" as thttp
            component "REST" as trest
            component "Common Library" as pcommon
            thttp - trest : use
            trest <-down- pcommon
            [Public Application] --> thttp
       }

       mcommon -[hidden]down- discovery
       bcommon -[hidden]down- discovery
       pcommon -[hidden]down- discovery

       mcommon -[hidden]down- identity
       bcommon -[hidden]down- identity
       pcommon -[hidden]down- identity

   }

   package "Services" as services {

        component "Microservice 1" as mone
        component "Microservice 2" as mtwo
        component "Microservice 3" as mthree
        component "Kafka" as kafka
        () "HTTP" as one_http
        () "HTTP" as two_http
        () "HTTP" as three_http

        mone - one_http
        mtwo - two_http
        mthree - three_http

        mone <-down-> kafka : event
        mtwo <-down-> kafka : event
        mthree <-down-> kafka : event

        identity -[hidden]down- mone
        identity -[hidden]down- mtwo
        identity -[hidden]down- mthree
   }

   package "Data" as data {
        database "RDBMS" as rdbms
        database "NoSQL" as nosql
        database "KeyValue" as keyvalue

        kafka -[hidden]down- rdbms
        kafka -[hidden]down- nosql
        kafka -[hidden]down- keyvalue
   }


.. toctree::
    :hidden:

    kubernetes
    docker
    api_gateway
    microservice
    cli