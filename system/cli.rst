Cli
===

el cli de minos es una herramienta que permite la creacion de codigo para los diferentes bloques o layer del framework

es una herramienta especifica para los desarolladores y permite mantener un estandar en la generacion y validacion
del codigo

.. code-block:: console

    $ minos


API Gateway Routers
-------------------

uno de los procesos mas importantes es la generacion de codigo para el Router del api Gateway.

para la generacion de las clases para el router es necesario pasar al cli un parametro que coresponde al nombre
del path

.. code-block:: console

    $ minos order -apig

este comando generará las clases para el api gateway, *order* y *orders*, cada clase tendrá su fichero especifico y
será creada dentro de la carpeta de las route.


es posible tambien pasar un parametro adicional

.. code-block:: console

    $ minos order -apig -no-plural

este comando generará solo una clase *order*

Herramientas
------------

Aunque el desarollador en cargo tiene la posibilidad de decidir el tipo de herramienta para

- crear el cli de API Engine
- sistema de templates para la generacion del codigo

se aconseja utilizar herramientas que ya estan destinadas a ese uso como cookie_cutter_ ,
cookie_cutter_python_, cookie_cutter_poetry_

.. warning::

   la generacion del cli tiene que ser por poetry_



.. _cookie_cutter: https://github.com/cookiecutter/cookiecutter
.. _cookie_cutter_python: https://github.com/audreyfeldroy/cookiecutter-pypackage
.. _cookie_cutter_poetry: https://github.com/briggySmalls/cookiecutter-pypackage
.. _poetry: https://python-poetry.org/