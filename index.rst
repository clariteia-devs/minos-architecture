.. minos documentation master file, created by
   sphinx-quickstart on Fri Jan  1 13:06:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
.. meta::
   :description: Python microservice architecture

.. title:: Minos the python microservice framework by Clariteia.

Welcome to Minos's Architecture documentation!
==============================================

Minos is a framework in Python 3 for Microservices.

El concepto principal es ofrecer una serie de herramientas para facilitar el desarollo de los servicios y crear,
en el limite de lo posible un codigo estandard.

En primer lugar es necesario tener cumplimentados los :doc:`Requisitos de Sistema </system/requirements>`


.. toctree::
   :includehidden:

   code_of_conduct
   system/architecture
   system/requirements